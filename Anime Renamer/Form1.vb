﻿Public Class Form1
    Private Sub browse_btn_Click(sender As Object, e As EventArgs) Handles browse_btn.Click
        Dim Directory
        Dim index As Integer
        Dim judul As String
        Dim files() As System.IO.FileInfo

        If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            path_box.Text = FolderBrowserDialog1.SelectedPath
            ListBox1.Items.Clear()

            Directory = path_box.Text
            Dim dirInfo As New System.IO.DirectoryInfo(Directory)
            files = dirInfo.GetFiles("*", IO.SearchOption.TopDirectoryOnly)
            For Each file In files
                ListBox1.Items.Add(file.ToString)
            Next

            index = path_box.Text.LastIndexOf("\")
            judul = path_box.Text.Substring(index + 1)
            Judul_box.Text = judul
            Episode_box.Text = 1
            My.Settings.Current.Clear()
            My.Settings.Before.Clear()
        End If
    End Sub

    Private Sub Increase_btn_Click(sender As Object, e As EventArgs) Handles Increase_btn.Click
        If IsNumeric(Episode_box.Text) Then
            Episode_box.Text += 1
        End If

    End Sub

    Private Sub Decrease_btn_Click(sender As Object, e As EventArgs) Handles Decrease_btn.Click
        If IsNumeric(Episode_box.Text) Then
            If Episode_box.Text > 1 Then
                Episode_box.Text -= 1
            End If
        End If
    End Sub

    Private Sub Episode_box_TextChanged(sender As Object, e As EventArgs) Handles Episode_box.TextChanged
        If IsNumeric(Episode_box.Text) Then
            If Episode_box.Text < 10 Then
                Judul_lbl.Visible = True
                Judul_lbl.Text = Judul_box.Text & " - " & "0" & Episode_box.Text
            Else
                Judul_lbl.Visible = True
                Judul_lbl.Text = Judul_box.Text & " - " & Episode_box.Text
            End If
        End If
    End Sub

    Private Sub Judul_box_TextChanged(sender As Object, e As EventArgs) Handles Judul_box.TextChanged
        If Episode_box.Text.Length = 2 Or Episode_box.Text.Length = 3 Or Episode_box.Text.Length = 4 Then
            Judul_lbl.Text = Judul_box.Text & " - " & Episode_box.Text
        Else
            Judul_lbl.Text = Judul_box.Text & " - " & "0" & Episode_box.Text
        End If
    End Sub

    Private Sub Episode_box_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Episode_box.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub ListBox1_DoubleClick(sender As Object, e As EventArgs) Handles ListBox1.DoubleClick
        Dim new_name As String
        Dim new_path As String
        If ListBox1.SelectedIndex > -1 And Judul_lbl.Text <> "" Then
            Dim selected As String = ListBox1.SelectedItem.ToString
            Dim index As Integer = selected.LastIndexOf(".")
            Dim ekstensi As String = selected.Substring(index)
            Dim path As String = path_box.Text & "/" & selected

            new_name = Judul_lbl.Text & ekstensi
            new_path = path_box.Text & "/" & new_name
            If System.IO.File.Exists(new_path) = False Then
                My.Computer.FileSystem.RenameFile(path, new_name)
                My.Settings.Current.Add(new_name)
                My.Settings.Before.Add(selected)
                ListBox1.Items.Insert(ListBox1.SelectedIndex, new_name)
                ListBox1.Items.Remove(ListBox1.SelectedItem)
                Episode_box.Text += 1
            Else
                Episode_box.Text += 1
            End If
        End If
    End Sub

    Private Sub Undo_btn_Click(sender As Object, e As EventArgs) Handles Undo_btn.Click
        If My.Settings.Current.Count > 0 And My.Settings.Before.Count > 0 Then
            Dim count As Integer = (ListBox1.Items.Count - 1)
            Dim index_akhir As Integer = (My.Settings.Current.Count - 1)
            Dim hola As String = My.Settings.Current.Item(index_akhir)
            Dim path As String
            Dim words As String

            For a = 0 To count
                words = ListBox1.Items.Item(a)
                If InStr(words.ToLower, hola.ToLower) Then
                    ListBox1.SelectedIndex = a
                    path = path_box.Text & "/" & ListBox1.SelectedItem.ToString
                    My.Computer.FileSystem.RenameFile(path, My.Settings.Before.Item(index_akhir))
                    ListBox1.Items.Insert(ListBox1.SelectedIndex, My.Settings.Before.Item(index_akhir))
                    ListBox1.Items.Remove(ListBox1.SelectedItem)
                    My.Settings.Before.RemoveAt(index_akhir)
                    My.Settings.Current.RemoveAt(index_akhir)
                    Episode_box.Text -= 1
                    ListBox1.SelectedIndex = a
                End If
            Next
        End If


    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        My.Settings.Current.Clear()
        My.Settings.Before.Clear()
    End Sub

    Private Sub Sort_btn_Click(sender As Object, e As EventArgs) Handles Sort_btn.Click
        ListBox1.Sorted = True
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim Selected As String = ListBox1.SelectedItem.ToString
        Process.Start(path_box.Text & "/" & Selected)

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) 

    End Sub
End Class
