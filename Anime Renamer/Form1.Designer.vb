﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.path_box = New System.Windows.Forms.TextBox()
        Me.browse_btn = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Sort_btn = New System.Windows.Forms.Button()
        Me.Decrease_btn = New System.Windows.Forms.Button()
        Me.Increase_btn = New System.Windows.Forms.Button()
        Me.Undo_btn = New System.Windows.Forms.Button()
        Me.Judul_lbl = New System.Windows.Forms.Label()
        Me.Judul_anime_lbl = New System.Windows.Forms.Label()
        Me.Episode_lbl = New System.Windows.Forms.Label()
        Me.Episode_box = New System.Windows.Forms.TextBox()
        Me.Judul_box = New System.Windows.Forms.TextBox()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.HorizontalScrollbar = True
        Me.ListBox1.ItemHeight = 16
        Me.ListBox1.Location = New System.Drawing.Point(12, 44)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(306, 324)
        Me.ListBox1.TabIndex = 0
        Me.ListBox1.TabStop = False
        Me.ListBox1.UseTabStops = False
        '
        'path_box
        '
        Me.path_box.Location = New System.Drawing.Point(12, 14)
        Me.path_box.Name = "path_box"
        Me.path_box.ReadOnly = True
        Me.path_box.Size = New System.Drawing.Size(233, 22)
        Me.path_box.TabIndex = 0
        Me.path_box.TabStop = False
        '
        'browse_btn
        '
        Me.browse_btn.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.browse_btn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.browse_btn.Location = New System.Drawing.Point(251, 13)
        Me.browse_btn.Name = "browse_btn"
        Me.browse_btn.Size = New System.Drawing.Size(67, 24)
        Me.browse_btn.TabIndex = 2
        Me.browse_btn.Text = "Browse"
        Me.browse_btn.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.Sort_btn)
        Me.GroupBox1.Controls.Add(Me.Decrease_btn)
        Me.GroupBox1.Controls.Add(Me.Increase_btn)
        Me.GroupBox1.Controls.Add(Me.Undo_btn)
        Me.GroupBox1.Controls.Add(Me.Judul_lbl)
        Me.GroupBox1.Controls.Add(Me.Judul_anime_lbl)
        Me.GroupBox1.Controls.Add(Me.Episode_lbl)
        Me.GroupBox1.Controls.Add(Me.Episode_box)
        Me.GroupBox1.Controls.Add(Me.Judul_box)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(336, 12)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(15, 3, 5, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(5)
        Me.GroupBox1.Size = New System.Drawing.Size(408, 194)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Setting"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(211, 162)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(60, 24)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Open"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Sort_btn
        '
        Me.Sort_btn.Location = New System.Drawing.Point(335, 162)
        Me.Sort_btn.Name = "Sort_btn"
        Me.Sort_btn.Size = New System.Drawing.Size(65, 24)
        Me.Sort_btn.TabIndex = 5
        Me.Sort_btn.Text = "Sort List"
        Me.Sort_btn.UseVisualStyleBackColor = True
        '
        'Decrease_btn
        '
        Me.Decrease_btn.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Decrease_btn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Decrease_btn.Location = New System.Drawing.Point(308, 89)
        Me.Decrease_btn.Name = "Decrease_btn"
        Me.Decrease_btn.Size = New System.Drawing.Size(25, 24)
        Me.Decrease_btn.TabIndex = 10
        Me.Decrease_btn.Text = "<"
        Me.Decrease_btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Decrease_btn.UseVisualStyleBackColor = True
        '
        'Increase_btn
        '
        Me.Increase_btn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Increase_btn.Location = New System.Drawing.Point(372, 89)
        Me.Increase_btn.Name = "Increase_btn"
        Me.Increase_btn.Size = New System.Drawing.Size(25, 24)
        Me.Increase_btn.TabIndex = 9
        Me.Increase_btn.Text = ">"
        Me.Increase_btn.UseVisualStyleBackColor = True
        '
        'Undo_btn
        '
        Me.Undo_btn.Location = New System.Drawing.Point(277, 162)
        Me.Undo_btn.Name = "Undo_btn"
        Me.Undo_btn.Size = New System.Drawing.Size(52, 24)
        Me.Undo_btn.TabIndex = 4
        Me.Undo_btn.Text = "Undo"
        Me.Undo_btn.UseVisualStyleBackColor = True
        '
        'Judul_lbl
        '
        Me.Judul_lbl.AutoSize = True
        Me.Judul_lbl.Location = New System.Drawing.Point(5, 33)
        Me.Judul_lbl.Name = "Judul_lbl"
        Me.Judul_lbl.Size = New System.Drawing.Size(101, 16)
        Me.Judul_lbl.TabIndex = 0
        Me.Judul_lbl.Text = "Judul - Episode"
        '
        'Judul_anime_lbl
        '
        Me.Judul_anime_lbl.AutoSize = True
        Me.Judul_anime_lbl.Location = New System.Drawing.Point(5, 65)
        Me.Judul_anime_lbl.Name = "Judul_anime_lbl"
        Me.Judul_anime_lbl.Size = New System.Drawing.Size(81, 16)
        Me.Judul_anime_lbl.TabIndex = 7
        Me.Judul_anime_lbl.Text = "Judul Anime"
        '
        'Episode_lbl
        '
        Me.Episode_lbl.AutoSize = True
        Me.Episode_lbl.Location = New System.Drawing.Point(305, 65)
        Me.Episode_lbl.Name = "Episode_lbl"
        Me.Episode_lbl.Size = New System.Drawing.Size(59, 16)
        Me.Episode_lbl.TabIndex = 6
        Me.Episode_lbl.Text = "Episode"
        '
        'Episode_box
        '
        Me.Episode_box.Location = New System.Drawing.Point(337, 90)
        Me.Episode_box.Name = "Episode_box"
        Me.Episode_box.Size = New System.Drawing.Size(31, 22)
        Me.Episode_box.TabIndex = 2
        Me.Episode_box.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Judul_box
        '
        Me.Judul_box.Location = New System.Drawing.Point(8, 90)
        Me.Judul_box.Name = "Judul_box"
        Me.Judul_box.Size = New System.Drawing.Size(289, 22)
        Me.Judul_box.TabIndex = 1
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(758, 381)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.browse_btn)
        Me.Controls.Add(Me.path_box)
        Me.Controls.Add(Me.ListBox1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Anime Renamer"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ListBox1 As ListBox
    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Friend WithEvents path_box As TextBox
    Friend WithEvents browse_btn As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Judul_lbl As Label
    Friend WithEvents Judul_anime_lbl As Label
    Friend WithEvents Episode_lbl As Label
    Friend WithEvents Episode_box As TextBox
    Friend WithEvents Judul_box As TextBox
    Friend WithEvents Decrease_btn As Button
    Friend WithEvents Increase_btn As Button
    Friend WithEvents Undo_btn As Button
    Friend WithEvents Sort_btn As Button
    Friend WithEvents Button1 As Button
End Class
